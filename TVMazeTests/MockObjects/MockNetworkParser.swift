//
//  MockNetworkParser.swift
//  TVMazeTests
//
//  Created by Cynthia Wiggins on 2019-10-14.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit
@testable import TVMaze


class MockNetworkParser: NetworkResponseParsable {
    func parseGetCastResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [CastMember] {
        throw TVMazeError.applicationError
    }
    
    
    var showtime: [Showtime]?
    var error: TVMazeError?
    var image: UIImage?
    
    func setResponse(showtime: [Showtime]?, error: TVMazeError?, image: UIImage?) {
        self.showtime = showtime
        self.error = error
        self.image = image
    }
    
    func parseGetEpisodesResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [Showtime] {
        if let error = error {
            throw error
        }
        
        guard let showtimes = showtime else {
            throw TVMazeError.applicationError
        }
        return showtimes
    }
    
    func parseGetImageResponse(data: Data?, response: URLResponse?, error: Error?) throws -> UIImage {
        if let error = error {
            throw error
        }
        
        guard let image = image else {
            throw TVMazeError.applicationError 
        }
        return image
    }
    
    
    
    
}

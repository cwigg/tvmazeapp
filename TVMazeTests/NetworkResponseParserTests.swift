//
//  NetworkResponseParserTests.swift
//  TVMazeTests
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import XCTest
@testable import TVMaze

class NetworkResponseParserTests: XCTestCase {
    
    var networkResponseParser: NetworkResponseParser!

    var jsonResponse: String = """

    [{"id":339141,"url":"http://www.tvmaze.com/episodes/339141/clarence-1x26-rough-riders-elementary","name":"Rough Riders Elementary","season":1,"number":26,"airdate":"2014-12-01","airtime":"07:30","airstamp":"2014-12-01T12:30:00+00:00","runtime":11,"image":null,"summary":"","show":{"id":5617,"url":"http://www.tvmaze.com/shows/5617/clarence","name":"Clarence","type":"Animation","language":"English","genres":["Comedy","Family"],"status":"Ended","runtime":15,"premiered":"2014-02-17","officialSite":null,"schedule":{"time":"16:00","days":["Monday","Tuesday","Wednesday","Thursday"]},"rating":{"average":null},"weight":0,"network":{"id":11,"name":"Cartoon Network","country":{"name":"United States","code":"US","timezone":"America/New_York"}},"webChannel":null,"externals":{"tvrage":null,"thetvdb":271421,"imdb":"tt3061050"},"image":{"medium":"http://static.tvmaze.com/uploads/images/medium_portrait/22/56691.jpg","original":"http://static.tvmaze.com/uploads/images/original_untouched/22/56691.jpg"},"summary":"<p>The series revolves around <b>Clarence</b>, an optimistic boy who wants to do everything because everything is amazing.</p>","updated":1533031317,"_links":{"self":{"href":"http://api.tvmaze.com/shows/5617"},"previousepisode":{"href":"http://api.tvmaze.com/episodes/1471923"}}},"_links":{"self":{"href":"http://api.tvmaze.com/episodes/339141"}}}]


    """
    
    var successData: Data? {
        return jsonResponse.data(using: .utf8)
    }
    
    override func setUp() {
        self.networkResponseParser = NetworkResponseParser()
    }


    override func tearDown() {}
}


//MARK: HTTPResponse Tests
extension NetworkResponseParserTests {
    
    func testHTTPStatusNotFoundThrowsNotFoundError() {
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 400, httpVersion: nil, headerFields: nil)
        
        do {
            let _ = try networkResponseParser.parseGetEpisodesResponse(data: nil, response: response, error: nil)
            XCTFail("HTTP Status Not Found Did Not Throw Error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.dataNotFound, "HTTP Not Found Should Throw NotFound Error")
        } catch {
            XCTFail("HTTP Status Not Found Threw Unknown Error")
        }
    }
    
    
    func testHTTPStatusServerErrorThrowsServerError() {
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 500, httpVersion: nil, headerFields: nil)
        
        do {
            let _ = try networkResponseParser.parseGetImageResponse(data: nil, response: response, error: nil)
            XCTFail("HTTP Status 500 Did Not Throw Error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.serverError, "HTTP Status 500 Did Not Throw Server Error")
        } catch {
            XCTFail("HTTP Status 500 Threw Unknown Error")
        }
    }
    
    func testMalformedResponse() {
        do {
            let _ = try networkResponseParser.parseGetImageResponse(data: nil, response: nil, error: nil)
            XCTFail("Malformed Response Should Throw InvalidAPIResponse Error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.invalidAPIResponseFormat, "Malformed Response Should Throw InvalidAPIResponse Error")
        } catch {
            XCTFail("Malformed Response Should Throw InvalidAPIResponse Error")
        }
    }
    
    func testErrorInResponseThrowsError() {
        do {
            let error = NSError(domain: "Test", code: -1001, userInfo: nil)
            let _ = try networkResponseParser.parseGetImageResponse(data: nil, response: nil, error: error)
            XCTFail("Error in response should throw error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.invalidAPIResponseFormat, "Error in response should throw error")
        } catch {
            XCTFail("Error in response should throw error")
        }
    }
    
    func testMissingDataThrowsError() {
        do {
            let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 200, httpVersion: nil, headerFields: nil)
            let _ = try networkResponseParser.parseGetImageResponse(data: nil, response: response, error: nil)
            XCTFail("Missing data should throw error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.invalidAPIResponseFormat, "Missing data should throw error")
        } catch {
            XCTFail("Missing data should throw TVMazeError")
        }
    }
    
    func testUnknownStatusResponseThrowsError() {
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 5000, httpVersion: nil, headerFields: nil)
        
        do {
            let _ = try networkResponseParser.parseGetImageResponse(data: nil, response: response, error: nil)
            XCTFail("HTTP Status 5000 Did Not Throw Error")
        } catch let error as TVMazeError {
            XCTAssert(error == TVMazeError.serverError, "HTTP Status 500 Did Not Throw Server Error")
        } catch {
            XCTFail("HTTP Status 500 Threw Unknown Error")
        }
    }
}


//MARK: Parse Get Episode Response Tests
extension NetworkResponseParserTests {
    
    func testSuccessfulGetEpisodeResponseReturnsShowtimes() {
        guard let jsonData = self.successData else {
            XCTFail("Could not format json data")
            return
        }
        
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        do {
            let showtimes = try networkResponseParser.parseGetEpisodesResponse(data: jsonData, response: response, error: nil)
            XCTAssert(showtimes.count == 1, "Response should return array of 1 showtime")
        } catch let error {
            XCTFail("Success threw error: \(error.localizedDescription)")
        }
    }
}


//MARK: Parse Get Image Response Tests
extension NetworkResponseParserTests {
    func testSuccessfulGetImageResponse() {
        guard let image = UIImage(named: "NoImageFound"),
            let imageData = image.pngData() else {
                XCTFail("Could not format json data")
                return
        }
        
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        do {
            let image = try networkResponseParser.parseGetImageResponse(data: imageData, response: response, error: nil)
            XCTAssertNotNil(image, "Response should return UIImage" )
        } catch let error {
            XCTFail("Success threw error: \(error.localizedDescription)")
        }
    }
    
    func testInvalidImageDataThrowsError() {
        guard let jsonData = self.successData else {
            XCTFail("Could not format json data")
            return
        }
        
        let response = HTTPURLResponse(url: URL(string: "https://www.testURL.ca")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        do {
            let _ = try networkResponseParser.parseGetImageResponse(data: jsonData, response: response, error: nil)
            XCTFail("Non image data should throw error")
        } catch let error {
            XCTAssert((error as? TVMazeError) == TVMazeError.invalidAPIResponseFormat, "Non image data should throw error" )
        }
    }
}

//
//  NetworkControllerTests.swift
//  TVMazeTests
//
//  Created by Cynthia Wiggins on 2019-10-14.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import XCTest
@testable import TVMaze

class NetworkControllerTests: XCTestCase {

    var networkController: NetworkController!
    
    override func setUp() {
        let mockNetworkParser = MockNetworkParser()
        self.networkController = NetworkController(with: mockNetworkParser)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct resultsD
    }

    
 

}


//MARK: Get Episode Tests
extension NetworkControllerTests {
    
    func testGetBaseTVMazeURL() {
        let baseTVMazeURL = networkController.baseTVMazeUrl.url
        guard let absoluteBaseURL = baseTVMazeURL?.absoluteString else {
            XCTFail("Url should not be nil")
            return
        }
        
        XCTAssert(absoluteBaseURL == "https://api.tvmaze.com", "Base URL Should be https")
    }
    

    //Example return: // = "https://api.tvmaze.com/schedule?country=US&date=2014-12-01"
    func testGetURLForDailyScheduleSuccess() {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        
        guard let date = formatter.date(from: "2019-10-01"),
            let dailyScheduleURL = networkController.getURLForDailySchedule(using: date) else {
                XCTFail("Url should not be nil")
                return
        }
        
        let expectedURL = "https://api.tvmaze.com/schedule?country=US&date=2019-10-01"
        XCTAssert(dailyScheduleURL.absoluteString == expectedURL, "Get url should return expected url")
    }
}


//MARK: Get Image Test
extension NetworkControllerTests {
    
    func testGetImageURLConvertsToHttps() {
        let httpURLString = "http://static.tvmaze.com/uploads/images/medium_portrait/126/316686.jpg"
        
        guard let httpsUrl = networkController.getImageURL(for: httpURLString) else {
            XCTFail("Url should not be nil")
            return
        }
        
        XCTAssert(httpsUrl.scheme == "https", "Get image url should convert http to https")
    }
    
    func testInvalidImageUrlReturnsNil() {
        let invalidImageUrl = "InvalidImageUrl"
        
        let imageUrl = networkController.getImageURL(for: invalidImageUrl)
        
        XCTAssert(imageUrl == nil, "Invalid image should return nil")
        
        if let _ = networkController.getImageURL(for: invalidImageUrl) {
            XCTFail("Invalid url should return nil")
            return
        }
    }
}

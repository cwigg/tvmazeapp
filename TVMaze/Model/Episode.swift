//
//  Episode.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 Decodable class  representing the Showtime returned by the TVMaze API
 */
class Showtime: Codable {
    let id: Int
    let url: String
    let name: String?
    let season: Int?
    let number: Int?
    let airstamp: String
    let runtime: Int?
    let show: Show
    
    /**
     Stores the image png data returned by the TVMaze API
     */
    var imagePngData: Data?
    
    /**
     Converts the imagePngData to a UIImage if the data is present. Otherwise returns the default 'No Image Found' image stored in the Assets
     */
    var episodeImage: UIImage {
        guard let imagePngData = imagePngData,
            let image = UIImage(data: imagePngData) else {
                return UIImage(named: "NoImageFound")!
        }
        
        return image
    }
    
    /**
     Converts the airstamp date string returned by the API to a Date object.
     Airstamp string example: "2019-10-14T02:10:00+00:00"
     */
    var airstampDate: Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter.date(from: airstamp)
    }
    
    /**
     Formats the airStampDate to display a given start time.
     Example Return: "5:30 PM"
     */
    var formattedStartTime: String? {
        guard let airstampDate = airstampDate else {
            return nil
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: airstampDate)
    }
    
    /**
     Formats the airStampDate to display a given start day.
     Example Return: "October 13, 2019"
     */
    var formattedAirDate: String? {
        guard let airstampDate = airstampDate else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        return dateFormatter.string(from: airstampDate)
    }
}



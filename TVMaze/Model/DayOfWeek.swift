//
//  DayOfWeek.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/** An enum refering to the Days of the Week with a String raw value.
 */
enum DayOfWeek: String, CaseIterable {
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
    case sunday = "Sunday"
    
    
    /**
     Returns an integer representiing the day of the week according to the Calendar object.
     - Returns: an integer representiing the day of the week
     */
    func getCalendarWeekdayInt() -> Int {
        switch self {
        case .sunday:
            return 1
        case .monday:
            return 2
        case .tuesday:
            return 3
        case .wednesday:
            return 4
        case .thursday:
            return 5
        case .friday:
            return 6
        case .saturday:
            return 7
        }
    }
    
    
    /**
     A static function to return the enum case corresponding to the numbered day of the week.
     - Parameter day: The day of the week
     - Returns: The enum case corresponding to the day of the week
     */
    static func getDayOfWeek(from day: Int) -> DayOfWeek {
        switch day {
        case 0:
            return sunday
        case 1:
            return monday
        case 2:
            return tuesday
        case 3:
            return wednesday
        case 4:
            return thursday
        case 5:
            return friday
        case 6:
            return saturday
        default:
            return sunday
        }
    }

    
    /**
     A static function to return the enum case corresponding to the inputted date.
     - Parameter date: A Date object
     - Returns: The enum case corresponding to the day of the week of the inputted date
     */
    static func getDayOfWeek(from date: Date) -> DayOfWeek {
        let currentCalendar = Calendar(identifier: .gregorian)
        let dayOfWeek = currentCalendar.component(.weekday, from: date)
        
        let updatedDayOfWeek = dayOfWeek - 1
        return getDayOfWeek(from: updatedDayOfWeek)
    }
}

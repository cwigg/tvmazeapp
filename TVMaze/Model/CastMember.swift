//
//  CastMember.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-16.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation


/**
Decodable struct  representing the CastMember object returned by the TVMaze API
*/
struct CastMember: Codable {
    var person: Person
    var character: Character
    
    /**
        A string containing the person's name and the character they play. Used for display purposes.
    */
    var starringAsCharacterString: String {
        return "\(person.name) as \(character.name)"
    }
}

/**
Decodable struct  representing the Person object returned by the TVMaze API. This is a parameter of CastMember
*/
struct Person: Codable {
    var id: Int
    var url: String
    var name: String
}

/**
Decodable struct  representing the Character object returned by the TVMaze API. This is a parameter of CastMember
*/
struct Character: Codable {
    var id: Int
    var url: String
    var name: String
}

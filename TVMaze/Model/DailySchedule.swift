//
//  DailySchedule.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
 Used to store an episode schedule for a given day of the week.
 */
class DailySchedule {
    
    /**
     The selected day of the week
     */
    let dayOfWeek: DayOfWeek
    
    /**
     The date of this schedule
     */
    var date: Date
    
    /**
     The episodes airing on this day. Will be nil until TVMaze API is called for this object
     */
    var episodeSchedule: [Showtime]?
    
    init(with dayOfWeek: DayOfWeek) {
        self.dayOfWeek = dayOfWeek
        self.date = Date()
        
        if  let date = getDateFromDayOfWeek() {
            self.date = date
        }
    }
    
    /**
     Identifies the date of the provided weekday for the current week of the year
     - Returns: The date of the provided weekday for the current week of the year
     */
    private func getDateFromDayOfWeek() -> Date? {
        let myCalendar = Calendar(identifier: .gregorian)
        let weekOfYear = myCalendar.component(.weekOfYear, from: Date())
        let year = myCalendar.component(.year, from: Date())
        let weekdayNumber = dayOfWeek.getCalendarWeekdayInt()
        
        let components = DateComponents(calendar: myCalendar,
                                        year: year,
                                        weekday: weekdayNumber,
                                        weekOfYear: weekOfYear)
        return myCalendar.date(from: components) 
    }
    
}

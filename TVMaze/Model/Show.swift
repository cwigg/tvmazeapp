//
//  Show.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-15.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/** A Codable object correspoding to the Show object returned by the TVMaze API.
 */
struct Show: Codable {
    
    let id: Int
    let url: String
    let name: String
    let type: String
    let language: String
    let genres: [String]
    let status: String
    let runtime: Int?
    let premiered: String
    let officialSite: String?
    let summary: String?
    let network: Network?
    let image: ShowImage?
}

/** A Codable object correspoding to the Network object returned by the TVMaze API. The Network object is returned as a parameter of a Show
 */
struct Network: Codable {
    let id: Int
    let name: String
}

/** A Codable object correspoding to the ShowImage object returned by the TVMaze API. The ShowImage object is returned as a parameter of a Show
 */
struct ShowImage: Codable {
    let medium: String
    let original: String
}

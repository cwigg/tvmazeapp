//
//  SceneDelegate.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    /* Note: UISplitViewController based on below tutorial:
    https://www.raywenderlich.com/4613809-uisplitviewcontroller-tutorial-getting-started */
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        
        guard
          let splitViewController = window?.rootViewController as? UISplitViewController,
          let leftNavController = splitViewController.viewControllers.first
            as? UINavigationController,
          let masterViewController = leftNavController.viewControllers.first
            as? WeekScheduleTableViewController,
        let rightNavController = splitViewController.viewControllers.last as? UINavigationController,
        let detailViewController = rightNavController.viewControllers.first
        as? DailyScheduleViewController
          else { fatalError() }

        //Inject dependencies into view controllers
        masterViewController.weekdaySelectionDelegate = detailViewController
        let networkResponseParser = NetworkResponseParser()
        let networkController = NetworkController(with: networkResponseParser)
        let dailySchedulePresenter = DailySchedulePresenter(with: networkController)
        
        detailViewController.configure(with: dailySchedulePresenter)
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}


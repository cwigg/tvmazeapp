//
//  UIViewControllerExtension.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 UIViewController extension used to display and hide an activity indicator in a view
 */
extension UIViewController {
    
    /**
     Creates a formatted UIActivityIndicatorView
     */
    private func getActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = UIColor.white
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }
    
    /**
     Creates a formatted background view for the activity indicator. The contrast between the activity indicator and the background makes the activity indicator more visible to the user
     */
    private func getIndicatorBackground() -> UIView {
        let indicatorWithBackground = UIView()
        indicatorWithBackground.layer.cornerRadius = 5
        indicatorWithBackground.tag = 999
        indicatorWithBackground.backgroundColor = UIColor.gray
        indicatorWithBackground.translatesAutoresizingMaskIntoConstraints = false
        return indicatorWithBackground
    }
    
    /**
     Attaches an activityIndicator with a grey background to the view and starts animating it. This activityIndicator is centered on the parent view, and an acitivity indicator will not be added to the parent view if one is already displayed.
     
     ActivityIndicator view is identified with the tag 999
     */
    func showActivityIndicator() {
        //Do not show activity indicator if one is already present on view
        if let _ = self.view.viewWithTag(999) {
            return
        }
        
        let activityIndicator = getActivityIndicator()
        let indicatorBackground = getIndicatorBackground()
        indicatorBackground.addSubview(activityIndicator)
        
        //Setup activity indicator constraints
        activityIndicator.centerYAnchor.constraint(equalTo: indicatorBackground.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: indicatorBackground.centerXAnchor).isActive = true
        indicatorBackground.widthAnchor.constraint(equalTo: activityIndicator.widthAnchor, constant: 30).isActive = true
        indicatorBackground.heightAnchor.constraint(equalTo: activityIndicator.heightAnchor, constant: 30).isActive = true
        
        
        DispatchQueue.main.async {
            self.view.addSubview(indicatorBackground)
            
            //Setup indicator background contraints
            indicatorBackground.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
            indicatorBackground.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            
            activityIndicator.startAnimating()
        }
    }
    
    /**
     Hides the activity indicator currently being displayed on the screen if one is present
     */
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            let activityIndicatorView = self.view.viewWithTag(999)
            activityIndicatorView?.removeFromSuperview()
        }
    }
}

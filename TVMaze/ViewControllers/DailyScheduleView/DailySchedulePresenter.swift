//
//  DailySchedulePresenter.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-14.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
    DailySchedulePresenter is used to provide data to the DailyScheduleViewController. It conforms to the DailySchedulePresentable protocol
*/
class DailySchedulePresenter: DailySchedulePresentable {
    /**
        currentlySelectedWeekday reflects the currently selected day of the week whose episodes are displayed in the DailyScheduleViewController.
        It is initialized to the current day of the week
    */
    var currentlySelectedWeekday: DayOfWeek
    
    /**
        An instance of NetworkControllable used to fetch data from the TVMazeAPI
    */
    private var networkController: NetworkControllable
    
    /**
        Contains the full list of showtimes for the currently selected day of the week. It is subdivided into sections based on distinct air times, and ordered by air time.
    */
    var allSortedShowtimes: [[Showtime]]?
    
    /**
        Contains the filtered results of the episode list. Is nil when no filter is active
    */
    var filteredSearchResults: [[Showtime]]?
    
    init(with networkController: NetworkControllable) {
        self.networkController = networkController
        self.currentlySelectedWeekday = DayOfWeek.getDayOfWeek(from: Date())
    }
}

//MARK: TableView Helpers
extension DailySchedulePresenter {
    /**
    getCountOfShowtimeSections fetches the number of sections in the list of showtimes. If a filter is active it returns the number of sections in the filtered result set, otherwise it returns the total number of sections in the episode list
     
     - Returns: The number of sections in the list of showtimes
    */
    func getCountOfShowtimeSections() -> Int {
                
        if let filteredSearchResults = filteredSearchResults {
            return filteredSearchResults.count
        }
        
        return allSortedShowtimes?.count ?? 0
    }
    
    /**
    Returns the header title for a given section.
     - Parameter section: The section index in a list of showtimes
     - Returns: The title of the selected section, or nil if the corresponding episode is not found
    */
    func getTitleForHeaderInSection(section: Int) -> String? {
        guard let showtimesForSection = allSortedShowtimes?[section],
            let firstShowtime = showtimesForSection.first,
            let startTime = firstShowtime.formattedStartTime else {
                return nil
        }
        return startTime
    }
    
    /**
    Returns the number of showtimes in a given section. If a filter is active it returns the number of showtimes in section in the filtered list, otherwise it refers to the total number of showtimes in the section
     - Parameter section: The section index in a list of showtimes
     - Returns: The number of showtimes in a given section
    */
    func getCountOfShowtimes(in section: Int) -> Int {
        if let filteredSearchResults = filteredSearchResults {
            return filteredSearchResults[section].count
        }
        return allSortedShowtimes?[section].count ?? 0
    }
    
    /**
    Fetches the showtime for the selected indexpath
     
     - Parameter indexPath: The indexPath of the selected cell
     - Returns: The showtime corresponding to that cell, or nil if that showtime is not found
    */
    func getShowtimeFor(indexPath: IndexPath) -> Showtime? {
        guard let showtimesToDisplay = self.filteredSearchResults ?? self.allSortedShowtimes else {
            return nil
        }
        return showtimesToDisplay[indexPath.section][indexPath.row]
    }
}

//MARK: Filter Helpers
extension DailySchedulePresenter {
    /**
    Determines whether the given showtime is in the result set of the given searchText. A showtime is in the result set if either its name, its show name or its network name contains the search text.
     
     - Parameter searchText: The search text used to filter results
     - Parameter showtime: The showtime being compared to the search text
     - Returns: A boolean indicating whether the showtime is in the filtered set
    */
    private func isShowInSearchResult(using searchText: String, showtime: Showtime) -> Bool {
        if let showName = showtime.name,
            showName.contains(searchText) {
            return true
        }
        
        if let network = showtime.show.network,
            network.name.contains(searchText) {
            return true
        }
        
        return showtime.show.name.contains(searchText)
    }
    
    /**
    Clears the search filters by setting the filtered search results to nil
    */
    func clearSearchFilters() {
        filteredSearchResults = nil
    }
    
    /**
    Filters the showtimes using the provided searchText and updated the variable filteredSearchResults accordingly. A showtime is in the result set if either its name, its show name or its network name contains the search text.
     
     - Parameter searchText:  The search text used to filter results
    */
    func filterShowtimes(using searchText: String) {
        let searchResults = allSortedShowtimes?.map { showTimeList -> [Showtime] in
            return showTimeList.filter{ showtime in
               return isShowInSearchResult(using: searchText, showtime: showtime)
            }
        }
        
        filteredSearchResults = searchResults?.filter { !$0.isEmpty }
    }
}

//MARK: Load Episodes
extension DailySchedulePresenter {
    
    /**
     loadShowtimesForDay fetches the sorted list of showtimes for the currentlySelectedWeekday
     
     - Returns: a closure containing an optional TVMazeError. This error will be present if the api returns an error or if there are no episodes found
     */
    func loadShowtimesForDay(closure: @escaping tvMazeErrorClosure) {
        filteredSearchResults = nil
        networkController.getEpisodesForDay(dayOfWeek: currentlySelectedWeekday) { showtimes, error in
            if let error = error {
                closure(error)
                return
            }
            guard let showtimes = showtimes,
                !showtimes.isEmpty else {
                    closure(TVMazeError.noEpisodesFound)
                    return
            }
            
            self.setShowtimes(with: showtimes)
            closure(nil)
        }
    }
    
    /**
     Sets and sorts the showtimes into sections based on airdates and sorts those subsections by air date descending.
     
     - Parameter showtimes: the list of showtimes for a given day
     */
    private func setShowtimes(with showtimes: [Showtime]) {
        let showAirDates = showtimes.map{ $0.airstampDate }
        let distinctShowtimes = Array(Set(showAirDates))
        
        allSortedShowtimes = distinctShowtimes.map { airDate -> [Showtime] in
            return showtimes.filter{ $0.airstampDate == airDate}
        }
        
        allSortedShowtimes?.sort(by: { showList1, showList2 in
            guard let firstAirTime = showList1.first?.airstampDate,
                let secondAirTime = showList2.first?.airstampDate else {
                    return true
            }
            return firstAirTime < secondAirTime
        })
    }
}

//MARK: Load Image
extension DailySchedulePresenter {
    /**
        Fetches the image for a given episode from the network controller
    - Parameter showtimeId: The unique identifier for the selected showtime
    - Returns: a closure containing an optional image and an optional error
    */
    func getEpisodeImage(for showtimeId: Int, getImageClosure: @escaping getImageResponse) {
        networkController.getEpisodeImage(for: showtimeId, dayOfWeek: currentlySelectedWeekday) { image, error in
            getImageClosure(image, error)
        }
    }
}

//MARK: Load Cast
extension DailySchedulePresenter {
    
    func getCastList(for showtimeId: Int, closure: @escaping getCastResponse) {
        networkController.getEpisodeCast(for: showtimeId) { castMembers, error in
            closure(castMembers, error)
        }
    }
}

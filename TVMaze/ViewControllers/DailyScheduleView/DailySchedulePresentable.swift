//
//  DailySchedulePresentable.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-15.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
    Protocol DailySchedulePresentable defines the functions used to provide data to the DailyScheduleViewController
*/
protocol DailySchedulePresentable {
    /**
        currentlySelectedWeekday: The day of the week whose data will be displayed
    */
    var currentlySelectedWeekday: DayOfWeek { get set }
    
    /**
        loadShowtimesForDay fetches the sorted list of showtimes for the currentlySelectedWeekday
     
     - Returns: a closure containing an optional TVMazeError
    */
    func loadShowtimesForDay(closure: @escaping tvMazeErrorClosure)
    
    /**
    getShowtimeFor fetches the showtime for the selected indexpath
     
     - Parameter indexPath: The indexPath of the selected cell
     - Returns: The showtime corresponding to that cell
    */
    func getShowtimeFor(indexPath: IndexPath) -> Showtime?
    
    /**
    getCountOfShowtimeSections fetches the number of sections in the list of showtimes.
     
     - Returns: The number of sections in the list of showtimes
    */
    func getCountOfShowtimeSections() -> Int
    
    /**
    getCountOfShowtimes
     - Parameter section: The section index in a list of showtimes
     - Returns: The number of showtimes in a given section
    */
    func getCountOfShowtimes(in section: Int) -> Int
    
    /**
    Returns the header title for a given section
     - Parameter section: The section index in a list of showtimes
     - Returns: The title of the selected section, or nil if the corresponding episode is not found
    */
    func getTitleForHeaderInSection(section: Int) -> String?
    
    /**
    Filters the list of showtimes using the provided searchText. The filtered results contain those whose name or network contains the searched text
     - Parameter searchText: The inputted search text used to filter the resutls
    */
    func filterShowtimes(using searchText: String)
    
    /**
        Clears all active search filters and resets the list of showtimes to contain all showtimes for the given day
    */
    func clearSearchFilters()
    
    /**
        Fetches the image for a given episode
     - Parameter showtimeId: The unique identifier for the selected showtime
    - Returns: a closure containing an optional image and an optional error
    */
    func getEpisodeImage(for showtimeId: Int, getImageClosure: @escaping getImageResponse)
    
    /**
     Fetches the cast list for a specific show
     - Parameter showtimeId: The unique identifier for the selected showtime
     - Returns: a closure containing an optional array of CastMembers and an optional error
     */
    func getCastList(for showtimeId: Int, closure: @escaping getCastResponse)
}

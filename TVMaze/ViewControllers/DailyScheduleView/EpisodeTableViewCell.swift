//
//  EpisodeTableViewCell.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 EpisodeTableViewCell is the formatted UITableViewCell used to display the list of showtimes on the DailyScheduleViewController
 */
class EpisodeTableViewCell: UITableViewCell {
    
    /**
     Initialized the Cell, ensuring that the cellStyle is set to contain a subtitle view
     */
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:  UITableViewCell.CellStyle.subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    /**
     Configures the cell with the showtime to be displayed. Removes the existing subviews from the cell and creates reformats cell for the new showtime. 
     
     - Parameter showtime: The showtime being displayed by the cell.
     */
    func configure(with showtime: Showtime) {
        contentView.subviews.forEach { $0.removeFromSuperview() }
        
        self.imageView?.image = showtime.episodeImage
        setupTitleLabel(for: showtime)
        formatDetailTextLabel(for: showtime)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Sets up the title label of the cell with the formatted showtime name. The title contains the show name, followed by the episode name. Displays "Unavailable" if the show name is not present
     
     - Parameter showtime: The showtime being displayed by the cell.
     */
    private func setupTitleLabel(for showtime: Showtime) {
        let showTitle = "\(showtime.show.name): \(showtime.name ?? "Unavailable")"
        textLabel?.attributedText = NSAttributedString(string: showTitle,
                                                       attributes: [NSAttributedString.Key.font: boldFont])
    }
    
    /**
     Sets up the detail label of the cell with additional information including air date and time and the network name.. Displays "Unavailable" if the data is not present
     
     - Parameter showtime: The showtime being displayed by the cell.
     */
    private func formatDetailTextLabel(for showtime: Showtime) {
        detailTextLabel?.numberOfLines = 0
        
        var detailText = "\(showtime.formattedStartTime ?? "Unavailable") on \(showtime.formattedAirDate ??  "Unavailable")"
        detailText.append("\n\(showtime.show.network?.name ?? "Unavailable")")
        
        detailTextLabel?.attributedText = NSAttributedString(string: detailText, attributes: [NSAttributedString.Key.font: regularFont])
    }
    
    /**
     Sets the image of the cell to the provided image if it is present.
     
     - Parameter image: The image being displayed to the left of the cell.
     */
    func setImage(with image: UIImage?) {
        if let episodeImage = image {
            DispatchQueue.main.async {
                self.imageView?.image = episodeImage
            }
        }
    }
}


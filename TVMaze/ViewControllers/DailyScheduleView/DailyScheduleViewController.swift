//
//  DailyScheduleViewController.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import UIKit

/**
    DailyScheduleViewController is a detail view controller used to display the episodes of a given day of the week. These episodes are displayed in a tableview ordered by airtime with separate headings for each distinct airtime. The data for this view controller is pulled from an instance of DailySchedulePresentable.
 
    DailyScheduleViewController also includes the ability to filter results through a search bar attached to the navigation bar. Tapping on episode cells displays an overlay with additional information
*/
class DailyScheduleViewController: UIViewController {
    
    /**
        Tableview for this viewController. Will be initialized on viewDidLoad
    */
    var episodeListTableView: UITableView!
    
    /**
        Presenter used to fetch and update data for this viewController. Must be initialized through configure function call.
    */
    var dailySchedulePresenter: DailySchedulePresentable!
    
    /**
        Sets up the view controller with an instance of DailySchedulePresentable used to fetch and update data
     
     **Configure must be called before DailyScheduleViewController viewDidLoad**
        Parameter dailySchedulePresenter: The presenter this view controller pulls its data from
    */
    func configure(with dailySchedulePresenter: DailySchedulePresentable) {
        self.dailySchedulePresenter = dailySchedulePresenter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupSearchBar()
        setupTableView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadUI()
    }
    
    /**
        Reloads Navigation Bar title and refreshes list of episodes for day
    */
    private func reloadUI() {
        setupNavigationBar()
        getDailySchedule()
        scrollTableViewToTop()
        setupSearchBar()
        
        //Setup back bar button
        let leftBarButton = splitViewController?.displayModeButtonItem
        leftBarButton?.tintColor = appColor
        navigationItem.leftBarButtonItem = leftBarButton
        navigationItem.leftItemsSupplementBackButton = true
    }
}

//MARK: Search Bar Delegate
extension DailyScheduleViewController: UISearchControllerDelegate,  UISearchResultsUpdating {
    /**
        Creates search controller and sets up delegates
    */
    private func setupSearchBar() {
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        searchController.delegate = self
        searchController.searchBar.placeholder = "Search for show or network"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
    }
    
    /**
        Filters the search results based on user input to the search bar and updates tableview
    */
    func updateSearchResults(for: UISearchController) {
        self.showActivityIndicator()
        
        guard let searchBarText = navigationItem.searchController?.searchBar.text,
            !searchBarText.isEmpty else {
            self.dailySchedulePresenter.clearSearchFilters()
            self.episodeListTableView.reloadData()
            self.hideActivityIndicator()
            return
        }
        
        self.dailySchedulePresenter?.filterShowtimes(using: searchBarText)
        self.episodeListTableView.reloadData()
        self.hideActivityIndicator()
    }
    
}

//MARK: UI Display
extension DailyScheduleViewController {
    /**
        Creates and adds the tableview to the main view, sets up the tableview's delegate and data source
    */
    private func setupTableView() {
        episodeListTableView = UITableView()
        self.view.addSubview(episodeListTableView)
        
        episodeListTableView.delegate = self
        episodeListTableView.dataSource = self
        episodeListTableView.register(EpisodeTableViewCell.self, forCellReuseIdentifier: "episodeCell")
        
        styleTableView()
    }
    
    /**
        Updates the styling of the tableview to cover the entire screen below the navigation bar, and remove the unused tableview cell lines in the footer
    */
    private func styleTableView() {
        episodeListTableView.tableFooterView = UIView()
        
        //Setup tableview contraints
        episodeListTableView.translatesAutoresizingMaskIntoConstraints = false
        episodeListTableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        episodeListTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        episodeListTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        episodeListTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        episodeListTableView.estimatedRowHeight = 100
    }
    
    /**
        Sets the title of the navigation bar to the currently selected weekday
    */
    private func setupNavigationBar() {
        title = dailySchedulePresenter?.currentlySelectedWeekday.rawValue
    }
    
    /**
        Generates a view for the header of a section in the tableview
        - Parameter headerTitle: The title of the header
     
        - Returns: The header view for the section
    */
    private func generateHeaderView(for headerTitle: String) -> UIView {
        let headerView = UILabel()
        headerView.backgroundColor = appColor
        
        let formattedHeaderTitle = NSAttributedString(string: headerTitle, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                                                        NSAttributedString.Key.font: boldFont])
        headerView.attributedText = formattedHeaderTitle
        headerView.textAlignment = .center
        return headerView
    }
    
    /**
        Displays an alert to the user containing the error description
        - Parameter error: The error to be displayed to the user
    */
    private func displayUserAlert(with error: TVMazeError) {
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
    
    /**
        Scrolls the tableview to display the episode at the top of the list.
    */
    private func scrollTableViewToTop() {
        guard dailySchedulePresenter.getCountOfShowtimeSections() > 0 else {
            return
        }
        
        let topCellIndexPath = IndexPath(row: 0, section: 0)
        episodeListTableView.scrollToRow(at: topCellIndexPath, at: .top, animated: true)
    }
}

// MARK: - Table view delegate
extension DailyScheduleViewController: UITableViewDelegate, UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dailySchedulePresenter.getCountOfShowtimeSections()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let startTime = dailySchedulePresenter.getTitleForHeaderInSection(section: section) else {
            return nil
        }
        return generateHeaderView(for: startTime)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailySchedulePresenter.getCountOfShowtimes(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let showtime = dailySchedulePresenter?.getShowtimeFor(indexPath: indexPath),
            let cell =  tableView.dequeueReusableCell(withIdentifier: "episodeCell", for: indexPath) as? EpisodeTableViewCell else {
                return UITableViewCell()
        }
        
        cell.configure(with: showtime)
        if showtime.imagePngData == nil {
            getEpisodeImage(for: cell, showtimeId: showtime.id)
        }
        
        return cell
    }
    
    /**
           Fetches the cast information from the api, then loads the detail overlay with the retrived show info
       */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedShowtime = dailySchedulePresenter?.getShowtimeFor(indexPath: indexPath) else {
            displayUserAlert(with: .dataNotFound)
            return
        }
        
        self.showActivityIndicator()
        dailySchedulePresenter.getCastList(for: selectedShowtime.show.id) { castMembers, error in
            self.hideActivityIndicator()
            self.displayEpisodeDetailOverlay(for: selectedShowtime, castMembers: castMembers)
        }
    }
    
    /**
     Displays an overlay containing episode details for the given show and cast members
     - Parameter showtime: the episode details to display
     - Parameter castMembers: An optional array of the show's cast members
     */
    private func displayEpisodeDetailOverlay(for showtime: Showtime, castMembers: [CastMember]?) {
        DispatchQueue.main.async {
            let episodeDetailOverlay = EpisodeDetailOverlayViewController()
            episodeDetailOverlay.configure(with: showtime, castMembers: castMembers)
            self.navigationController?.modalPresentationStyle = .overCurrentContext
            self.present(episodeDetailOverlay, animated: true)
        }
    }
}

//MARK: Data Fetch
extension DailyScheduleViewController {
    /**
        Fetches the episode's image and updates the cell accordingly
        - Parameter cell: the episode's cell in the tableview
        - Parameter showtimeId: the episode's unique identifier
    */
    private func getEpisodeImage(for cell: EpisodeTableViewCell, showtimeId: Int) {
        dailySchedulePresenter.getEpisodeImage(for: showtimeId) { image, error in
            cell.setImage(with: image)
        }
    }
    
    /**
        Fetches all episodes for the currently selected weekday and reloads tableview
    */
    private func getDailySchedule() {
        self.showActivityIndicator()
        dailySchedulePresenter?.loadShowtimesForDay() { error in
            self.hideActivityIndicator()
            if let error = error {
                self.displayUserAlert(with: error)
                return
            }
            
            DispatchQueue.main.async {
                self.episodeListTableView.reloadData()
            }
        }
    }
}


//MARK: WeekdaySelectionDelegate
extension DailyScheduleViewController: WeekdaySelectionDelegate {
    /**
     Updates the currently displayed day of the week on the detail view controller
        - Parameter weekday: The newly selected day of the week
    */
    func didSelectWeekday(weekday: DayOfWeek) {
        self.dailySchedulePresenter.currentlySelectedWeekday = weekday
        reloadUI()
    }
}

//
//  EpisodeDetailOverlayViewController.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 EpisodeDetailOverlayViewController is an overlay used to display additional information about a given show. This overlay can be dismissed by swiping down.
 */
class EpisodeDetailOverlayViewController: UIViewController {
    
    /**
     The showtime whose details are being displayed in the overlay
     */
    var showtime: Showtime?
    
    /**
     The cast member object whose details will be displayed in overlay if present
     */
    var castMembers: [CastMember]?
    
    func configure(with showtime: Showtime, castMembers: [CastMember]?) {
        self.showtime = showtime
        self.castMembers = castMembers
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupView()
    }
    
    /**
     Programatically creates and adds all labels to the view. Sets up layout constraints
     */
    private func setupView() {
        guard let showtime = showtime else {
            displayNoInformationAvailableLabel()
            return
        }
        
        //Add swipeIndicator
        let swipeIndicator = addSwipeIndicatorToView()
        
        //TODO: (cw) Add showtimeDetailsContainer in scroll view
        let showtimeDetailsContainer = UIView()
        showtimeDetailsContainer.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(showtimeDetailsContainer)
        
        showtimeDetailsContainer.topAnchor.constraint(equalTo: swipeIndicator.topAnchor, constant: 20).isActive = true
        showtimeDetailsContainer.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        showtimeDetailsContainer.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        //Add show name label
        let formattedShowName = NSAttributedString(string: showtime.show.name,
                                                   attributes: [.font: boldFont])
        let showNameLabel = addLabelToView(with: formattedShowName, parentView: showtimeDetailsContainer)
        showNameLabel.centerYAnchor.constraint(equalTo: showtimeDetailsContainer.centerYAnchor).isActive = true
        
        
        //Add episode name label and constraints
        if let episodeName = showtime.name {
            let formattedEpisodeName = NSAttributedString(string: episodeName, attributes: [.font: boldFont])
            let episodeNameLabel = addLabelToView(with: formattedEpisodeName, parentView: showtimeDetailsContainer)
            episodeNameLabel.topAnchor.constraint(equalTo: showNameLabel.bottomAnchor).isActive = true
        }
        
        //Add episode Image view
        let episodeImageView = generateEpisodeImageView(using: showtime.episodeImage, parent: showtimeDetailsContainer)
        episodeImageView.topAnchor.constraint(greaterThanOrEqualTo: swipeIndicator.bottomAnchor
            , constant: 20).isActive = true
        episodeImageView.bottomAnchor.constraint(equalTo: showNameLabel.topAnchor, constant: -25).isActive = true
        
        //Add season and episode label
        let seasonAndEpisodeNumberLabel = generateSeasonAndEpisodeLabel(with: showtimeDetailsContainer)
        seasonAndEpisodeNumberLabel.topAnchor.constraint(equalTo: showNameLabel.bottomAnchor, constant: 25).isActive = true
        
        
        if let runtime = showtime.runtime {
            let runtimeLabelText = NSAttributedString(string: "Runtime: \(runtime)", attributes: [.font:  UIFont.systemFont(ofSize: 12)])
            let runtimeLabel = addLabelToView(with: runtimeLabelText, parentView: showtimeDetailsContainer)
            runtimeLabel.topAnchor.constraint(equalTo: seasonAndEpisodeNumberLabel.bottomAnchor, constant: 5).isActive = true
        }
        
        //Add genre label to view
        let allGenres = showtime.show.genres.joined(separator: ", ")
        let genreLabelText = NSAttributedString(string: "Genre: \(allGenres)", attributes: [.font:  UIFont.systemFont(ofSize: 12)])
        let genreLabel = addLabelToView(with: genreLabelText, parentView: showtimeDetailsContainer)
        genreLabel.topAnchor.constraint(equalTo: seasonAndEpisodeNumberLabel.bottomAnchor, constant: 30).isActive = true
        
        var summaryTopAnchorView = genreLabel
        
        //Add formatted castMembers label
        if let castMembers = castMembers {
            let castMemberLabel = generateCastMemberLabel(with: castMembers, parentView: showtimeDetailsContainer)
            castMemberLabel.topAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 10).isActive = true
            summaryTopAnchorView = castMemberLabel
        }
        
        //Add formatted summary label
        let summaryLabel = generateSummaryLabel(for: showtime.show.summary ?? "")
        showtimeDetailsContainer.addSubview(summaryLabel)
        summaryLabel.centerXAnchor.constraint(equalTo: showtimeDetailsContainer.centerXAnchor).isActive = true
        summaryLabel.widthAnchor.constraint(equalTo: showtimeDetailsContainer.widthAnchor, constant: -(UIScreen.main.bounds.width * 0.1)).isActive = true
        summaryLabel.topAnchor.constraint(equalTo: summaryTopAnchorView.bottomAnchor, constant: 10).isActive = true
    }
    
    private func generateEpisodeImageView(using episodeImage: UIImage, parent: UIView) -> UIImageView {
        let episodeImageView = UIImageView(image: episodeImage)
        parent.addSubview(episodeImageView)
        episodeImageView.translatesAutoresizingMaskIntoConstraints = false
        episodeImageView.centerXAnchor.constraint(equalTo: parent.centerXAnchor).isActive = true
        return episodeImageView
    }
    
    
    private func addLabelToView(with text: NSAttributedString, parentView: UIView) -> UILabel {
        let label = UILabel()
        label.attributedText = text
        parentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
        return label
    }
    
}

//MARK: Season and Episode Label
extension EpisodeDetailOverlayViewController {
    
    func generateSeasonAndEpisodeLabel(with parentView: UIView) -> UILabel {
        let seasonAndEpisodeText = NSMutableAttributedString()
        if let episodeNumber = showtime!.number {
            let formattedEpisodeText = NSAttributedString(string: "Episode \(episodeNumber)", attributes: [.font:  UIFont.systemFont(ofSize: 12)])
            seasonAndEpisodeText.append(formattedEpisodeText)
        }
        
        if let seasonNumber = showtime!.season {
            let formattedSeasonText = NSAttributedString(string: "  Season \(seasonNumber)", attributes: [.font:  UIFont.systemFont(ofSize: 12)])
            seasonAndEpisodeText.append(formattedSeasonText)
        }
        
        
        let seasonAndEpisodeNumberLabel = addLabelToView(with: seasonAndEpisodeText, parentView: parentView)
        return seasonAndEpisodeNumberLabel
    }
}

//MARK: Cast Member Label
extension EpisodeDetailOverlayViewController {
    /**
     Formats the title and creates the label for the Cast Member property
     - Returns: The generated  cast member label
     */
    func generateCastMemberLabel(with castMembers: [CastMember], parentView: UIView) -> UILabel {
        let starringAsStrings = castMembers.map{$0.starringAsCharacterString}
        let castMembersStarringString = "Starring: \(starringAsStrings.joined(separator: ", "))"
        let castMemberString = NSAttributedString(string: castMembersStarringString, attributes: [.font: UIFont.systemFont(ofSize: 12)])
        let castMemberLabel = addLabelToView(with: castMemberString, parentView: parentView)
        castMemberLabel.numberOfLines = 0
        castMemberLabel.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.8).isActive = true
        return castMemberLabel
    }
}

//MARK: Summary Label
extension EpisodeDetailOverlayViewController {
    
    /**
     Generates a summary label with formatted multi-line HTML display.
     - Returns: The  formatted summary label
     */
    private func generateSummaryLabel(for summary: String) -> UILabel {
        let summaryLabel = UILabel()
        summaryLabel.numberOfLines = 0
        summaryLabel.lineBreakMode = .byWordWrapping
        
        summaryLabel.attributedText = getFormattedSummaryString(from: summary)
        summaryLabel.translatesAutoresizingMaskIntoConstraints = false
        
        return summaryLabel
    }
    
    /**
     Formats the summary from html to an NSAttributedString
     - Returns: The formatted attributed String
     */
    private func getFormattedSummaryString(from summary: String?) -> NSAttributedString? {
        guard let summary = summary,
            let summaryData = summary.data(using: .utf8) else {
                return nil
        }
        
        let formattedAttributedString = try? NSAttributedString(data: summaryData,
                                                                options: [.documentType: NSAttributedString.DocumentType.html,
                                                                          .characterEncoding:String.Encoding.utf8.rawValue],
                                                                documentAttributes: nil)
        return formattedAttributedString
    }
}


//MARK: Swipe Indicator Setup
extension EpisodeDetailOverlayViewController {
    /**
     Adds the swipe indicator to the overlay view and formats constraints
     - Returns: The generated swipe indicator
     */
    private func addSwipeIndicatorToView() -> UIView {
        let swipeIndicator = generateSwipeIndicator()
        self.view.addSubview(swipeIndicator)
        swipeIndicator.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        swipeIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        return swipeIndicator
    }

    /**
     Generates the swipe icon visible at the top of the overlay
     */
    private func generateSwipeIndicator() -> UIView {
        let swipeIndicator = UIView()
        swipeIndicator.translatesAutoresizingMaskIntoConstraints = false
        swipeIndicator.heightAnchor.constraint(equalToConstant: 7).isActive = true
        let screenSizeWidth = UIScreen.main.bounds.width
        swipeIndicator.widthAnchor.constraint(equalToConstant: screenSizeWidth * 0.15).isActive = true
        swipeIndicator.layer.cornerRadius = 3
        swipeIndicator.backgroundColor = appColor
        
        return swipeIndicator
    }
}


//MARK: Display No Information Available
extension EpisodeDetailOverlayViewController {
    
    /**
     Displays a label indicating that no information is available when the showtime is missing or improperly configured
     */
    private func displayNoInformationAvailableLabel() {
        let noInformationAvailableLabel = UILabel()
        noInformationAvailableLabel.text = "No additional information available"
        self.view.addSubview(noInformationAvailableLabel)
        noInformationAvailableLabel.translatesAutoresizingMaskIntoConstraints = false
        noInformationAvailableLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        noInformationAvailableLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
}

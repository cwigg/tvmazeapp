//
//  WeekScheduleTableViewController.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import UIKit

/**
 WeekScheduleTableViewController is a master view controller used to display the days of the week. Tapping on a day of the week will display the list of those episodes in the detail view controller (DailyScheduleViewController).
 The days of the week are displayed from Monday to Sunday.
 */
class WeekScheduleTableViewController: UITableViewController {
    /**
     The delegate used to set the selected day of the week in the detail view controller
     */
    var weekdaySelectionDelegate: WeekdaySelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Schedule"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.tableFooterView = UIView()
    }
}

// MARK: - Table view data source
extension WeekScheduleTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  DayOfWeek.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    /**
     Returns the formatted tableViewCell for the given day of the week. The cells alternate background colors for added contrast
     */
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekdayCell", for: indexPath)
        let dayOfWeek = DayOfWeek.allCases[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        
        if indexPath.row.isMultiple(of: 2) {
            cell.backgroundColor = fadedAppColor
            cell.textLabel?.attributedText = NSAttributedString(string: dayOfWeek.rawValue,
                                                                attributes: whiteBoldFontAttributes)
        } else {
            cell.backgroundColor = traitCollection.userInterfaceStyle == .dark ? UIColor.black: UIColor.white
            cell.textLabel?.attributedText = NSAttributedString(string:  dayOfWeek.rawValue, attributes: blueBoldFontAttributes)
        }
        
        return cell
    }
    
    /**
     Selecting a row updates the day of the week in the view controller's WeekdaySelectionDelegate. For smaller screens without the split screen it displays the detail view controller
     */
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let weekdaySelectionDelegate = weekdaySelectionDelegate else {
            return
        }
        
        let selectedWeekday = DayOfWeek.allCases[indexPath.row]
        weekdaySelectionDelegate.didSelectWeekday(weekday: selectedWeekday)
        
        if let dailyScheduleViewController = weekdaySelectionDelegate as? DailyScheduleViewController {
            splitViewController?.showDetailViewController(dailyScheduleViewController, sender: nil)
        }
    }
}


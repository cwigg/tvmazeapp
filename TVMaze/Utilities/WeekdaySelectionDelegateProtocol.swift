//
//  WeekdaySelectionDelegateProtocol.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
 Protocol to update the day of the week in delegate
*/
protocol WeekdaySelectionDelegate: class {
    /**
     Updates the currently displayed day of the week on the detail view controller
        - Parameter weekday:The newly selected day of the week
    */
    func didSelectWeekday(weekday: DayOfWeek)
}

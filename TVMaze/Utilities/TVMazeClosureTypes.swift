//
//  TVMazeClosureTypes.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-15.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
    Defines closure types used in asynchronous calls to pass data back to presenter and view controller
*/
typealias getDailyScheduleResponse = (([Showtime]?, TVMazeError?) -> ())
typealias getImageResponse = ((UIImage?, TVMazeError?) -> ())
typealias tvMazeErrorClosure = ((TVMazeError?) -> ())
typealias getCastResponse = (([CastMember]?, TVMazeError?) -> ())

//
//  TVMazeStyles.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-14.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
    Font and color styles used throughout the app
*/
let appColor = UIColor(red: 0.04, green: 0.15, blue: 0.71, alpha: 1)
let fadedAppColor = UIColor(red: 0.04, green: 0.15, blue: 0.71, alpha: 0.6)


let boldFont = UIFont.boldSystemFont(ofSize: 16.0)
let regularFont = UIFont.systemFont(ofSize: 16.0)

let whiteBoldFontAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                              NSAttributedString.Key.font: boldFont]
let blueBoldFontAttributes:  [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: appColor,
                                                              NSAttributedString.Key.font: boldFont]

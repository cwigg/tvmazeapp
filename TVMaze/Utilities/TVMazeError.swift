//
//  TVMazeError.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
 Enum to contain custom errors that can occur in this app.
*/
enum TVMazeError: Error {
    /**
        Thrown when there is no network available to make an api call
    */
    case networkNotAvailable
    
    /**
        Application error thrown to user when app cannot function as expected
    */
    case applicationError
    
    /**
        Thrown when no episodes are found for a given day
    */
    case noEpisodesFound
    
    /**
        Thrown when the api call returns a 500 HTTP status
    */
    case serverError
    
    /**
        Thrown when the api response is in an invalid or unexpected format
    */
    case invalidAPIResponseFormat
    
    /**
        Thrown when the api call returns a 400 HTTP status, or when the tableview cannot find the related data
    */
    case dataNotFound //http 404 error and when cell data can't be displayed in overlay
    
    /**
       Localized error descriptions for each case
    */
    var localizedDescription: String {
        switch self {
        case .networkNotAvailable:
            return "Could not connect to network. Please connect to network to refresh data"
        case .applicationError:
            return "Application Error"
        case .noEpisodesFound:
            return "Could not find schedule for selected day"
        case .serverError:
            return "Server Error"
        case .invalidAPIResponseFormat:
            return "API Response in unexpected format"
        case .dataNotFound:
            return "Could not load data"
        }
    }
}

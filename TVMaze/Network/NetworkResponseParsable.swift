//
//  NetworkResponseParsable.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-15.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 Protocol defining the functions used to parse API responses
 */
protocol NetworkResponseParsable {
    /**
     Parses the response from the Schedule endpoint of the TVMaze API.
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: The list of showtimes returned by the api on success
     - Throws: TVMaze Errors for non 200 HTTP statuses, or decoder errors
     */
    func parseGetEpisodesResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [Showtime]
    
    /**
     Parses the response from the get Image  endpoint of the TVMaze API.
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: The episode image on success
     - Throws: TVMaze Errors for non 200 HTTP statuses, or decoder errors
     */
    func parseGetImageResponse(data: Data?, response: URLResponse?, error: Error?) throws -> UIImage
    
    
    /**
     Parses the response from the get Cast  endpoint of the TVMaze API.
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: An array of cast members on success
     - Throws: TVMaze Errors for non 200 HTTP statuses, or decoder errors
     */
    func parseGetCastResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [CastMember]
}

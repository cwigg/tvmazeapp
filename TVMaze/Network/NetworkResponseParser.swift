//
//  NetworkResponseParser.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-13.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 Protocol defining the functions used to parse API responses
 */
class NetworkResponseParser: NetworkResponseParsable {
    //MARK: Common Utilities
    
    /**
     Parses common API responses to extract data. Throws an error if the API returns an error, parses the HTTP status code and returns the data on success
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: The data returned by the API
     - Throws: TVMaze Errors for non 200 HTTP statuses, or the error returned by the API
     */
    private func extractDataFromResponse(data: Data?, response: URLResponse?, error: Error?) throws -> Data {
        if let error = error {
            throw error
        }
        
        try parseUrlResponse(response: response)
        guard let data = data else {
            throw TVMazeError.invalidAPIResponseFormat
        }
        
        return data
    }
    
    /**
     Parses the urlResponse to check the HTTP status code. Throws an error when status code not in 200 range.
     - Parameter response: The url Response returned by the api (optional)
     - Throws: TVMaze Errors for non 200 HTTP statuses
     */
    private func parseUrlResponse(response: URLResponse?) throws {
        guard let httpResponse = response as? HTTPURLResponse else {
            throw TVMazeError.invalidAPIResponseFormat
        }
        
        switch httpResponse.statusCode {
        case 200...299:
            return
        case 400...499:
            throw TVMazeError.dataNotFound
        case 500...599:
            throw TVMazeError.serverError
        default:
            throw TVMazeError.invalidAPIResponseFormat
        }
    }
}

//MARK: Parse Get Episode Response
extension NetworkResponseParser {
    /**
     Parses the response from the Schedule endpoint of the TVMaze API.
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: The list of showtimes returned by the api on success
     - Throws: TVMaze Errors for non 200 HTTP statuses, or decoder errors
     */
    func parseGetEpisodesResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [Showtime] {
        let data = try extractDataFromResponse(data: data, response: response, error: error)
        return try self.decodeGetEpisodeResponse(from: data)
    }
    
    /**
     Decodes the data returned by the API into an array of Showtime objects. Throws an error when decoding attempt fails.
     - Parameter data: The data returned by the api
     - Returns: An array of showtime objects representing the schedule for a day
     - Throws: Decoder error when data cannot be decoded into [Showtime]
     */
    private func decodeGetEpisodeResponse(from data: Data) throws -> [Showtime] {
        let decoder = JSONDecoder()
        let episodeList = try decoder.decode([Showtime].self, from: data)
        return episodeList
    }
}

//MARK: Parse Get Image Response
extension NetworkResponseParser {
    /**
     Parses the response from the get Image  endpoint of the TVMaze API.
     - Parameter data: The data returned by the api (optional)
     - Parameter response: The url Response returned by the api (optional)
     - Parameter error: The error returned by the api (optional)
     - Returns: The episode image on success
     - Throws: TVMaze Errors for non 200 HTTP statuses, or decoder errors
     */
    func parseGetImageResponse(data: Data?, response: URLResponse?, error: Error?) throws -> UIImage {
        let data = try extractDataFromResponse(data: data, response: response, error: error)
        guard let image = UIImage(data: data) else {
            throw TVMazeError.invalidAPIResponseFormat
        }
        
        return image
    }
}


//MARK: Parse Get Cast Response
extension NetworkResponseParser {
    
    func parseGetCastResponse(data: Data?, response: URLResponse?, error: Error?) throws -> [CastMember] {
        let data = try extractDataFromResponse(data: data, response: response, error: error)
        return try decodeGetCastResponse(from: data)
    }
    
    private func decodeGetCastResponse(from data: Data) throws -> [CastMember] {
        let decoder = JSONDecoder()
        let castList = try decoder.decode([CastMember].self, from: data)
        return castList
    }
}

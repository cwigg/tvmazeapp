//
//  NetworkControllable.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-15.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation

/**
    Protocol definiing the functions used to fetch episode and image data from the API
*/
protocol NetworkControllable {
    /**
       Fetches the episodes aired on a given day of the week.
     - Parameter dayOfWeek: The selected day of the week the episode schedules should be fetched for
     - Returns: A closure containing an optional Showtime and an optional Error
    */
    func getEpisodesForDay(dayOfWeek: DayOfWeek, closure: @escaping getDailyScheduleResponse)
    
    /**
       Fetches the episode image for a given show on a given day of the week
     - Parameter dayOfWeek: The selected day of the week the episode image should be fetched for
    - Parameter showtimeId: The unique identifier for show
     - Returns: A closure containing an optional image and an optional error.
    */
    func getEpisodeImage(for showtimeId: Int, dayOfWeek: DayOfWeek, closure: @escaping getImageResponse)
    
    /**
     Fetches the  cast list for a given show
     - Parameter showtimeId: The unique identifier for show
     - Returns: A closure containing an optional array of cast members and an optional error.
     */
    func getEpisodeCast(for showtimeId: Int, closure: @escaping getCastResponse)
}

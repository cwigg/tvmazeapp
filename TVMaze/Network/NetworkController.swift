//
//  NetworkController.swift
//  TVMaze
//
//  Created by Cynthia Wiggins on 2019-10-12.
//  Copyright © 2019 Cynthia Wiggins. All rights reserved.
//

import Foundation
import UIKit

/**
 Fetches episode and schedule data from the TVMazeAPI
 */
class NetworkController: NetworkControllable {
    
    /**
     An array of 7 schedules corresponding to the days of the week. Used to store fetched episode data.
     */
    let weeklySchedule = DayOfWeek.allCases.map { day in
        return DailySchedule(with: day)
    }
    
    /**
     The base TVMazeUrl. Confroms to https scheme
     */
    var baseTVMazeUrl : URLComponents {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.tvmaze.com"
        return urlComponents
    }
    
    /**
     Used to parse api responses
     */
    let networkParser: NetworkResponseParsable
    
    init(with networkParser: NetworkResponseParsable) {
        self.networkParser = networkParser
    }
}

//MARK: Get Showtimes Call
extension NetworkController {
    
    /**
     Formats  the api endpoint used to retrive the episode schedule for a given date. Url is based on the baseTVMazeAPI.
     - Parameter date: The date of the desired episode schedule
     - Returns: The formatted url to retrive the schedule for the given date, or nil if the url could not be properly formatted
     
     */
    func getURLForDailySchedule(using date: Date) -> URL? {
        var urlComponent = baseTVMazeUrl
        urlComponent.path = "/schedule"
        let countryQueryItem = URLQueryItem(name: "country", value: "US")
        let dateString = getFormattedDateForURLQuery(for: date)
        let dateQueryItem = URLQueryItem(name: "date", value: dateString) 
        urlComponent.queryItems = [countryQueryItem, dateQueryItem]
        return urlComponent.url
    }
    
    /**
     Formats  the date for the api endpoint used to retrive the episode schedule. Example result: "2019-10-13"
     - Parameter date: The date of the desired episode schedule
     - Returns: The formatted date to be used in the url to retrive the schedule for that date
     
     */
    func getFormattedDateForURLQuery(for date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    /**
     Determines whether the stored daily schedule refers to the current week's data. Returns true if the data refers to the current week's data and false if the data refers to another week and needs to be refreshed.
     - Parameter dailySchedule: The currently stored daily schedule
     - Returns: A boolean indicating whether the schedule  is up to date
     
     */
    private func isScheduleUpToDate(for dailySchedule: DailySchedule) -> Bool {
        let myCalendar = Calendar(identifier: .gregorian)
        
        let storedWeekOfYear  = myCalendar.component(.weekOfYear, from: dailySchedule.date)
        let storedYear = myCalendar.component(.year, from: dailySchedule.date)
        
        let currentWeekOfYear = myCalendar.component(.weekOfYear, from: Date())
        let currentYear = myCalendar.component(.year, from: Date())
        
        return (storedWeekOfYear, storedYear) == (currentWeekOfYear, currentYear)
    }
    
    /**
     Retries the episodes for the selected day of the week. If that day's data has already been retrived from the api, it returns the stored list of episodes, otherwise it calls the TVMaze API to fetch the data
     - Parameter dayOfWeek: The day of the week the schedule should be returned for
     - Returns: A  closure containing a list of showtimes if they can be returned, or a TVMazeError otherwise
     
     */
    func getEpisodesForDay(dayOfWeek: DayOfWeek, closure: @escaping getDailyScheduleResponse) {
        guard let dailySchedule = weeklySchedule.first(where: { $0.dayOfWeek == dayOfWeek}) else {
            //Weekly schedule should initialize all days on class init
            closure(nil, TVMazeError.applicationError)
            
            return
        }
        
        //Return stored schedule if data is present and up to date
        if let showTimesForDay = dailySchedule.episodeSchedule,
            isScheduleUpToDate(for: dailySchedule) {
            closure(showTimesForDay, nil)
            return
        }
        
        guard let url = getURLForDailySchedule(using: dailySchedule.date) else {
            //Could not format url
            closure(nil, TVMazeError.applicationError)
            return
        }
        
        let showtimesDataTask =  URLSession.shared.dataTask(with: url) { (data , response , error) in
            do {
                let showtimes = try self.networkParser.parseGetEpisodesResponse(data: data, response: response, error: error)
                dailySchedule.episodeSchedule = showtimes
                closure(dailySchedule.episodeSchedule, nil)
            } catch let error as TVMazeError {
                closure(nil, error)
                return
            } catch {
                closure(nil, TVMazeError.applicationError)
                return
            }
        }
        
        showtimesDataTask.resume()
    }
}

//MARK: Get Image Call
extension NetworkController {
    /**
     Formats the image url to use https instead of http
     - Parameter imageUrl: The image url attached to a given episode
     - Returns: The formatted url using https, or nil if the image url could not be formatted
     
     */
    func getImageURL(for imageUrl: String) -> URL? {
        guard let url = URL(string: imageUrl),
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)  else {
                return nil
        }
        
        urlComponents.scheme = "https"
        return urlComponents.url
    }
    
    /**
     Fetches the image from the TVMaze API using the https version of the url provided by the showtime object. On success, updates the showtime object to include the new image.
     - Parameter showtimeId: The unique id associated with a given showtime
     - Parameter dayOfWeek: The day of the week the episode is scheduled on
     - Returns: A closure containing an image if if could be fetched from the api, or an error if the api call failed.
     
     */
    func getEpisodeImage(for showtimeId: Int, dayOfWeek: DayOfWeek, closure: @escaping getImageResponse) {
        
        guard  let dailySchedule = weeklySchedule.first(where: { $0.dayOfWeek == dayOfWeek }),
            let filteredShowtimes = dailySchedule.episodeSchedule?.filter({ $0.id == showtimeId}),
            let showtime = filteredShowtimes.first,
            let imageUrlString = showtime.show.image?.medium,
            let imageUrl = getImageURL(for: imageUrlString) else {
                closure(nil, TVMazeError.applicationError)
                return
        }
        
        let imageURLDataTask =  URLSession.shared.dataTask(with: imageUrl) { (data , response , error) in
            
            do {
                let image = try self.networkParser.parseGetImageResponse(data: data, response: response, error: error)
                showtime.imagePngData = image.pngData()
                closure(image, nil)
            } catch let error as TVMazeError {
                closure(nil, error)
                return
            } catch {
                closure(nil, TVMazeError.applicationError)
                return
            }
        }
        
        imageURLDataTask.resume()
    }
}

//MARK: Get Cast Information
extension NetworkController {
    /**
     Generates a formatted cast url based on the show id
     - Parameter showtimeId: The unique id associated with a given showtime
     - Returns: The formatted url using https
     
     */
    private func getCastUrl(for showId: Int) -> URL? {
        var urlComponent = baseTVMazeUrl
        urlComponent.path = "/shows/\(showId)/cast"
        return urlComponent.url
    }
    
    /**
     Fetches the cast information from the TVMaze API for a given show. On success returns the first 5 cast members returned by the API since the API sorts cast members by importance.
     - Parameter showtimeId: The unique id associated with a given showtime
     - Returns: A closure containing an array of cast members if if could be fetched from the api, or an error if the api call failed.
     
     */
    func getEpisodeCast(for showtimeId: Int, closure: @escaping getCastResponse) {
        guard let castUrl = getCastUrl(for: showtimeId) else {
            closure(nil, TVMazeError.applicationError)
            return
        }
        
        let castURLDataTask =  URLSession.shared.dataTask(with: castUrl) { (data , response , error) in
            
            do {
                
                let castMembers = try self.networkParser.parseGetCastResponse(data: data,
                                                                              response: response,
                                                                              error: error)
                //Returns 5 most important cast members
                let mostImportantCast = Array(castMembers.prefix(5))
                closure(mostImportantCast, nil)
            } catch let error as TVMazeError {
                closure(nil, error)
                return
            } catch {
                closure(nil, TVMazeError.applicationError)
                return
            }
        }
        
        castURLDataTask.resume()
    }
}

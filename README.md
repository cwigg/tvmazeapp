# TVMazeApp

This TVMaze App displays episode schedules from the TVMaze API. It supports multiple screen resolutions from iphones to iPads. 

This app was build on Xcode 11.1, macOS Mojave

## Implementation

This app uses a master-detail organization to support multiple screen sizes. The master view controller (WeeklyScheduleViewController) includes a list of days of the week, tapping on any of these opens the detail view (DailyScheduleiewController) which contains a list of episodes subdivided and sorted by start time. The detail view also includes a search bar attached to the navigation bar. The data for the detail view is passed through the DailySchedulePresenter in order to further modularize data and reduce dependencies. Episode images are fetched from the API as part of the cellForRowAt function in the detail view. The image may load after the rest of the cell has appeared on screen and defaults to a 'No Image Available' image.

This app uses dependency injection in the SceneDelegate class. All dependencies in a given class are protocols to modularize the code and make unit test implementation easier. 

Tapping on any cell of the detail view brings up the episode detail overlay which includes an enlarged episode poster, as well as additional information about the show, including casts information, genre, duration and a summary of the episode.

All API calls are made asynchronously with an activity indicator being displayed on the screen until the call is completed. The activity indicator setup can be found in the UIViewControllerExtension.

The API calls are made through the NetworkController class which ensures that all calls are made as https calls to conform to Apple's App Transport Security. The NetworkController class caches the data returned by the API to reduce the number of network calls. This data is refreshed on app launch, or when this week's schedule becomes outdated. 

All network responses are parsed through the NetworkResponseParser class. The parser analyzes the HTTP status response, then uses Codable objects to decode the API responses.

## Tutorials Consulted
* UISplitViewController implementation was based on: 
  https://www.raywenderlich.com/4613809-uisplitviewcontroller-tutorial-getting-started
* UISearchController setup and implementation losely based on tutorial at:
  https://www.raywenderlich.com/4363809-uisearchcontroller-tutorial-getting-started

## Image Sources
All images and assets are free to use for personal projects. 

The No-image-available image is found here: https://commons.wikimedia.org/wiki/File:No-image-available.png 

The logo was generated from a 'Create Your Own Logo' site found here: https://www.ucraft.com/free-logo-maker

